name := "Iceberg"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.1"

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.6.0"
