import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.storage.StorageLevel

object Iceberg {

  def main(args: Array[String]) {

    val inputPath = args(0);
    val outputPath = args(1);

    val conf = new SparkConf().setAppName("Iceberg");
    val sc = new SparkContext(conf);
    val data = sc.textFile(inputPath);
    val data_mod1 = data.map(x => x.toLowerCase().replace("."," ").replace("-", " ").replace(","," ").replace("?"," "));
    val data_mod2 = data_mod1.map(x => x.replace(";"," ").replace(":"," ").replace("_"," ").replace("#"," ").replace("!"," "));
    val words = data_mod2.flatMap(_.split(" "));
    val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _);
    val newTupleWord = wordCounts.map(process1);
    newTupleWord.persist();

    val distinctSR =  newTupleWord.map(_._3).distinct;
    val listSR = distinctSR.map(x => x).collect;
    var countSR = distinctSR.count();
    var finalResult = new ArrayBuffer[String]();
 
    for  (i <- 0 to countSR.toInt - 1){
       val temp = newTupleWord.filter(_._3 == listSR(i));
       val result = temp.take((listSR(i)*temp.count()/100).toInt);
       val resultFormatted = result.map(formatResult);
       for (j <- 0 to resultFormatted.length - 1) finalResult.append(resultFormatted(j));
    }
    
     val resultTupleWord= finalResult.map(process2);
     val icebergResult = resultTupleWord.sortBy(_._2).reverse.take(300);
     val output = sc.parallelize(icebergResult);
     output.saveAsTextFile(outputPath);
  }

 def process1(x: (String, Int)): Tuple3[String, Int, Float] = {
    var samplingrate : Float = 0;
    val word = x._1;
    val count = x._2.toInt;

    if (count >= 4000 )
    {samplingrate = 100}
    else
    {samplingrate = 100*count /(count + 4000);}

    new Tuple3(word, count, samplingrate);
 }
  
 def formatResult(x: Tuple3[String, Int, Float]): String = {
   x._1 + "," + x._2 + "," + x._3;
 }
 
 def process2(x: (String)): Tuple3[String, Int, Float] = {
	val tokens = x.split(",");
	val word = tokens(0);
	val countValue = tokens(1).toInt;
	val samplingRatio = tokens(2).toFloat;
	new Tuple3(word, countValue, samplingRatio);
 }
 
}

